//
//  TokenManager.swift
//  dabaandriver
//
//  Created by Nikolay Druzianov on 03.12.2017.
//  Copyright © 2017 Nikolay Druzianov. All rights reserved.
//

import UIKit
import CoreData

class TokenManager {
    
    func saveToken (value: String!) -> Bool {
        let context = getContext()
        let entity =  NSEntityDescription.entity(forEntityName: "Token", in: context)
        
        let fetchRequest: NSFetchRequest<Token> = Token.fetchRequest()
        do {
            //go get the results
            let array_tokens = try getContext().fetch(fetchRequest)
            
            //You need to convert to NSManagedObject to use 'for' loops
            for token in array_tokens as [NSManagedObject] {
                //get the Key Value pairs (although there may be a better way to do that...
                context.delete(token)
            }
            //save the context
            do {
                try context.save()
                print("TokenManager: Данные токена удалены из устройства")
            } catch let error as NSError  {
                print("TokenManager: Невозможно удалить данные токена \(error), \(error.userInfo)")
            } catch {
                
            }
        } catch {
            print("TokenManager: Ошибка запроса при удалении: \(error)")
        }
        
        let token = NSManagedObject(entity: entity!, insertInto: context)
        
        //set the entity values
        token.setValue(value, forKey: "value")
        
        var isSaved = false
        
        //save the object
        do {
            try context.save()
            isSaved = true
            print("TokenManager: Токен сохранен в памяти устройства")
        } catch let error as NSError  {
            print("TokenManager: Невозможно сохранить токен. Код ошибки: \(error), \(error.userInfo)")
            isSaved = false
        } catch {
            
        }
        
        if isSaved {
            return true
        } else {
            return false
        }
    }
    
    func loadToken () -> String {
        let fetchRequest: NSFetchRequest<Token> = Token.fetchRequest()
        
        do {
            //go get the results
            let array_tokens = try getContext().fetch(fetchRequest)
            if (array_tokens.count > 0) {
                let token = array_tokens[0] as NSManagedObject
                let tokenString = token.value(forKey: "value") as! String
                
                print ("TokenManager: Токен в памяти устройства: \(token.value(forKey: "value")!)")
                
                return tokenString
            } else {
                return ""
            }
        } catch {
            print("TokenManager: Ошибка запроса при загрузке токена: \(error)")
            return ""
        }
    }
    
    
    func getContext () -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
