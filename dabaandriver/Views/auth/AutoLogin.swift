//
//  ViewController.swift
//  dabaandriver
//
//  Created by Nikolay Druzianov on 03.12.2017.
//  Copyright © 2017 Nikolay Druzianov. All rights reserved.
//

import UIKit

class AutoLogin: UIViewController {

    var token = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tokenManager = TokenManager()
        self.token = tokenManager.loadToken()
        if self.token != ""  {
            print("Login: Токен найден \(self.token). Запуск проверки токена...")
            checkLogin(token: self.token)
        } else {
            print("Login: Токен не найден. Переход в Login.swift")
            goToLogin()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func showErrID(err_id: Int) {
        
        var err_message = ""
        
        switch err_id {
        case 1:
            err_message = "Не указан номер телефона"
        case 2:
            err_message = "Повторите попытку"
        case 3:
            err_message = "Ошибка работы токена"
        case 4:
            err_message = "Заполните все поля"
        case 5:
            err_message = "Ваш аккаунт неактивен"
        case 6:
            err_message = "Ваш аккаунт не существует или не активен"
        case 7:
            err_message = "Заказ закрыт, или отменен"
        case 8:
            err_message = "Недостаточно средств на балансе"
        case 9:
            err_message = "Заказ уже принят"
        default: break
        }
        
        let alert = UIAlertController(title: "Ошибка", message: err_message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "ОК", style: UIAlertActionStyle.default, handler: { (action: UIAlertAction!) in
            self.goToLogin()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func goToLogin() {
        DispatchQueue.main.async {
            print("AutoLogin: Переход на Login.swift")
            let login = self.storyboard?.instantiateViewController(withIdentifier: "Login") as! Login
            self.present(login, animated: false, completion: nil)
        }
    }
    
    func runCheckLogin(){
        checkLogin(token: self.token)
    }
    
    func checkLogin(token: String?) {
        
        let url = URL(string: "http://rvcode.ru/mobile/drv_autologin")!
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        let postString = "token=\(token!)"
        request.httpBody = postString.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("Login: error=\(String(describing: error))")
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Ошибка запроса к серверу", message: "Отсутствует интернет подключение", preferredStyle: .alert)
                    let action = UIAlertAction(title: "ПОВТОРИТЬ", style: UIAlertActionStyle.cancel,handler: {
                        action in self.runCheckLogin()
                    })
                    alert.addAction(action)
                    self.present(alert, animated: true, completion:nil)
                }
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("Login: statusCode should be 200, but is \(httpStatus.statusCode)")
                print("Login: response = \(String(describing: response))")
                DispatchQueue.main.async {
                    self.runCheckLogin()
                }
            } else {
                //print("data: \(data)")
                let jsonResult = try! JSONSerialization.jsonObject(with: data, options: [])
                print("Login: Ответ запроса к /drv_autologin = \(jsonResult)")
                let rowData: NSDictionary = jsonResult as! NSDictionary
                let err_id = rowData["err_id"] as! Int
                
                if err_id == 0 {
                    let order_id = String(describing: rowData["order_id"] as! NSObject)
                    print("Login: Токен рабочий")
                    if order_id == "0" {
                        print("AutoLogin: Нет активных заказов. Переход в OrdersList.swift")
                        DispatchQueue.main.async {
                            let ordersList = self.storyboard?.instantiateViewController(withIdentifier: "OrdersList") as! OrdersList
                            self.present(ordersList, animated: false, completion: nil)
                        }
                    } else {
                        print("Login: Есть активный заказ. ID заказа: \(order_id)")
                        print("Login: Переход в OrderSearch.swift")
                        DispatchQueue.main.async {
                            let takenOrder = self.storyboard?.instantiateViewController(withIdentifier: "TakenOrder") as! TakenOrder
                            takenOrder.order_id = order_id
                            self.present(takenOrder, animated: false, completion: nil)
                        }
                    }
                } else {
                    DispatchQueue.main.async {
                        self.showErrID(err_id: err_id)
                    }
                }
            }
        }
        task.resume()
    }
}

