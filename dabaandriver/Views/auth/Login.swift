//
//  Login.swift
//  dabaandriver
//
//  Created by Nikolay Druzianov on 03.12.2017.
//  Copyright © 2017 Nikolay Druzianov. All rights reserved.
//

import UIKit

class Login: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var phoneTxt: UITextField!
    @IBOutlet weak var passTxt: UITextField!
    @IBOutlet weak var enterButton: UIButton!
    @IBOutlet weak var logoTxt: UILabel!
    @IBOutlet weak var logoDescriptionTxt: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func showErrID(err_id: Int) {
        
        var err_message = ""
        
        switch err_id {
        case 1:
            err_message = "Не указан номер телефона"
        case 2:
            err_message = "Повторите попытку"
        case 3:
            err_message = "Ошибка работы токена"
        case 4:
            err_message = "Заполните все поля"
        case 5:
            err_message = "Ваш аккаунт неактивен"
        case 6:
            err_message = "Ваш аккаунт не существует или не активен"
        case 7:
            err_message = "Заказ закрыт, или отменен"
        case 8:
            err_message = "Недостаточно средств на балансе"
        case 9:
            err_message = "Заказ уже принят"
        default: break
        }
        
        let alert = UIAlertController(title: "Ошибка", message: err_message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "ОК", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField!) -> Bool {
        textField.resignFirstResponder()
        if (textField == self.phoneTxt) {
            self.passTxt.becomeFirstResponder()
        } else {
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        moveTextField(textField: textField, moveDistance: -210, up: true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        moveTextField(textField: textField, moveDistance: -210, up: false)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.phoneTxt {
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= 11 // Bool
        } else {
            return true
        }
    }
    
    func moveTextField (textField: UITextField, moveDistance: Int, up: Bool) {
        let moveDuration = 0.3;
        let movement: CGFloat = CGFloat(up ? moveDistance : -moveDistance)
        if up {
            self.logoDescriptionTxt.isHidden = true
            self.logoTxt.isHidden = true
        } else {
            self.logoDescriptionTxt.isHidden = false
            self.logoTxt.isHidden = false
        }
        UIView.beginAnimations("animateTextField", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(moveDuration)
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
    }
    
    func checkForErrors() -> Bool {
        var errors = false
        let title = "Ошибка"
        var message = ""
        if (self.phoneTxt.text?.isEmpty)! {
            errors = true
            message += "Введите номер телефона"
            alertWithTitle(title: title, message: message, ViewController: self, toFocus:self.phoneTxt)
            
        }
        else if (self.passTxt.text?.isEmpty)!
        {
            errors = true
            message += "Введите пароль"
            alertWithTitle(title: title, message: message, ViewController: self, toFocus:self.passTxt)
        }
        return errors
    }
    
    func alertWithTitle(title: String!, message: String, ViewController: UIViewController, toFocus:UITextField) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel,handler: {_ in
            toFocus.becomeFirstResponder()
        });
        alert.addAction(action)
        ViewController.present(alert, animated: true, completion:nil)
    }
    
    @IBAction func enterTapped(_ sender: UIButton) {
        sender.isEnabled = false
        if !checkForErrors() {
            let login = self.phoneTxt.text!
            let pass = self.passTxt.text!
            drvLogin(login: login, pass: pass)
        } else {
            sender.isEnabled = true
        }
    }
    
    func runDrvLogin() {
        let login = self.phoneTxt.text!
        let pass = self.passTxt.text!
        drvLogin(login: login, pass: pass)
    }
    
    func drvLogin(login: String!, pass: String!) {
        
        let url = URL(string: "http://rvcode.ru/mobile/drvlogin")!
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        let postString = "phone=\(login!)&pass=\(pass!)"
        print("\(postString)")
        request.httpBody = postString.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                DispatchQueue.main.async {
                    
                    let alert = UIAlertController(title: "Ошибка запроса к серверу", message: "Отсутствует интернет подключение", preferredStyle: .alert)
                    let action = UIAlertAction(title: "ПОВТОРИТЬ", style: UIAlertActionStyle.cancel,handler: {
                        action in self.runDrvLogin()
                    })
                    alert.addAction(action)
                    self.present(alert, animated: true, completion:nil)
                }
                
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(String(describing: response))")
                
                DispatchQueue.main.async {
                    self.runDrvLogin()
                }
                
            } else {
                let jsonResult = try! JSONSerialization.jsonObject(with: data, options: [])
                print("Login: Ответ на запрос к /drvlogin = \(jsonResult)")
                let rowData: NSDictionary = jsonResult as! NSDictionary
                let err_id = rowData["err_id"] as! Int
                if err_id == 0 {
                    DispatchQueue.main.async {
                        let tokenManager = TokenManager()
                        let token = rowData["token"] as! String
                        if tokenManager.saveToken(value: token) {
                            print("Login: Переход к OrdersList.swift")
                            let mainPage = self.storyboard?.instantiateViewController(withIdentifier: "OrdersList") as! OrdersList
                            self.present(mainPage, animated: false, completion: nil)
                        } else {
                            //ERROR TOKEN SAVE
                            self.enterButton.isEnabled = true
                        }
                    }
                } else {
                    //ERROR ID
                    DispatchQueue.main.async {
                        self.showErrID(err_id: err_id)
                        self.enterButton.isEnabled = true
                    }
                }
            }
        }
        task.resume()
    }
    
}
