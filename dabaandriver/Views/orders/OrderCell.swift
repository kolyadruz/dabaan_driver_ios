//
//  OrderCell.swift
//  dabaandriver
//
//  Created by Nikolay Druzianov on 03.12.2017.
//  Copyright © 2017 Nikolay Druzianov. All rights reserved.
//

import UIKit

class OrderCell: UITableViewCell {

    @IBOutlet weak var nameTxt: UILabel!
    @IBOutlet weak var pointAtxt: UILabel!
    @IBOutlet weak var pointBtxt: UILabel!
    @IBOutlet weak var noteTxt: UILabel!
    @IBOutlet weak var costTxt: UILabel!
    
    var order_id = Int()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCell(id: Int, point_a: String, point_b: String, price: String, prim: String) {
        self.order_id = id
        
        self.pointAtxt.text = point_a
        self.pointBtxt.text = point_b
        self.costTxt.text = price
        self.noteTxt.text = prim
        
    }

}
