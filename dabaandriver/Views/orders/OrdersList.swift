//
//  OrdersList.swift
//  dabaandriver
//
//  Created by Nikolay Druzianov on 03.12.2017.
//  Copyright © 2017 Nikolay Druzianov. All rights reserved.
//

import UIKit

class OrdersList: UIViewController, UITableViewDelegate, UITableViewDataSource {

    let theme = ThemeManager.currentTheme()
    
    @IBOutlet weak var ordersTableView: UITableView!
    
    var refresh = true
    var token = ""
    var ordersArray = NSMutableArray()
    var selectedOrderID = 0;
    
    @IBOutlet weak var balanceTxt: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = theme.backgroundColor
        
        self.ordersTableView.isHidden = true
    }

    override func viewDidAppear(_ animated: Bool) {
        let tokenManager = TokenManager()
        self.token = tokenManager.loadToken()
        getOrdersList(token: self.token)
        self.refresh = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        print("OrdersList: Dissapeared")
        self.refresh = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func showErrID(err_id: Int) {
        
        var err_message = ""
        
        switch err_id {
        case 1:
            err_message = "Не указан номер телефона"
        case 2:
            err_message = "Повторите попытку"
        case 3:
            err_message = "Ошибка работы токена"
        case 4:
            err_message = "Заполните все поля"
        case 5:
            err_message = "Ваш аккаунт неактивен"
        case 6:
            err_message = "Ваш аккаунт не существует или не активен"
        case 7:
            err_message = "Заказ закрыт, или отменен"
        case 8:
            err_message = "Недостаточно средств на балансе"
        case 9:
            err_message = "Заказ уже принят"
        default: break
        }
        
        let alert = UIAlertController(title: "Ошибка", message: err_message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "ОК", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! OrderCell
        let title = cell.pointAtxt.text
        let message = cell.costTxt.text
        self.selectedOrderID = cell.order_id
        
        let actionSheetController: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        
        let closeAB = UIAlertAction(title: "Закрыть", style: .cancel) { _ in
            print("closed")
        }
        actionSheetController.addAction(closeAB)
        
        let addAB = UIAlertAction(title: "Принять", style: .default)
        { _ in
            print("adding")
            self.runTakeOrder()
        }
        actionSheetController.addAction(addAB)
        
        /*let hideAB = UIAlertAction(title: "Скрыть", style: .default)
        { _ in
            print("hiding")
            self.runHideOrder()
        }
        actionSheetController.addAction(hideAB)*/
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.ordersArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: OrderCell = tableView.dequeueReusableCell(withIdentifier: "OrderCell") as! OrderCell
        
        let order = self.ordersArray[indexPath.row] as! NSDictionary
        
        cell.setCell(
            id: Int((String(describing: order["id"] as! NSObject) as NSString).intValue),
            point_a: String(describing: order["point_a"] as! NSObject),
            point_b: String(describing: order["point_b"] as! NSObject),
            price: String(describing: order["price"] as! NSObject),
            prim: String(describing: order["prim"] as! NSObject)
        )
        
        return cell
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        let height: CGFloat = 150.0
        
        return height
    }
    
    func getOrdersListWithInterval() {
        Timer.scheduledTimer(timeInterval: 1,
                             target: self,
                             selector: #selector(runGetOrdersList),
                             userInfo: nil,
                             repeats: false)
    }
    
    @objc func runGetOrdersList() {
        self.getOrdersList(token: self.token)
    }
    
    func runTakeOrder(){
        takeOrder(token: self.token, order_id: self.selectedOrderID)
    }
    
    func getOrdersList(token: String!) {
        
        let url = URL(string: "http://rvcode.ru/mobile/orderslist")!
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        let postString = "token=\(token!)"
        //print("\(postString)")
        request.httpBody = postString.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                DispatchQueue.main.async {
                    
                    let alert = UIAlertController(title: "Ошибка запроса к серверу", message: "Отсутствует интернет подключение", preferredStyle: .alert)
                    let action = UIAlertAction(title: "ПОВТОРИТЬ", style: UIAlertActionStyle.cancel,handler: {
                        action in self.runGetOrdersList()
                    })
                    alert.addAction(action)
                    self.present(alert, animated: true, completion:nil)
                }
                
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(String(describing: response))")
                
                DispatchQueue.main.async {
                    self.runGetOrdersList()
                }
                
            } else {
                
                let jsonResult = try! JSONSerialization.jsonObject(with: data, options: [])
                //print("OrdersList: Ответ на запрос к /orderslist = \(jsonResult)")
                let rowData: NSDictionary = jsonResult as! NSDictionary
                let err_id = rowData["err_id"] as! Int
                if err_id == 0 {
                    //print("Orders/OrdersList: Список успешно получен")
                    DispatchQueue.main.async {
                        let balance = String(describing: rowData["balance"] as! NSObject)
                        self.balanceTxt.text = "Баланс \(balance) руб"
                        
                        self.ordersArray.removeAllObjects()
                        let orders:NSArray = rowData["orders"] as! NSArray
                        
                        if orders.count > 0 {
                            
                            for order in orders {
                                //let data: NSDictionary = order as! NSDictionary
                                self.ordersArray.add(order)
                            }
                            
                        } else {
                            
                        }
                        
                        let contentOffset: CGPoint = self.ordersTableView.contentOffset;
                        self.ordersTableView.reloadData()
                        self.ordersTableView.layoutIfNeeded()
                        self.ordersTableView.setContentOffset(contentOffset, animated: true)
                        
                        if orders.count > 0 {
                            self.ordersTableView.isHidden = false
                        } else {
                            self.ordersTableView.isHidden = true
                        }
                        
                    }
                } else {
                    /*ERROR*/
                    print("OrdersList: Ошибка err_id???")
                }
                
                DispatchQueue.main.async {
                    if self.refresh {
                        self.getOrdersListWithInterval()
                    }
                }
            }
        }
        task.resume()
    }
    
    func takeOrder(token: String!, order_id: Int!) {
        
        let url = URL(string: "http://rvcode.ru/mobile/takeorder")!
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        let postString = "token=\(token!)&order_id=\(order_id!)"
        print("\(postString)")
        request.httpBody = postString.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                DispatchQueue.main.async {
                    
                    let alert = UIAlertController(title: "Ошибка запроса к серверу", message: "Отсутствует интернет подключение", preferredStyle: .alert)
                    let action = UIAlertAction(title: "ПОВТОРИТЬ", style: UIAlertActionStyle.cancel,handler: {
                        action in self.runTakeOrder()
                    })
                    alert.addAction(action)
                    self.present(alert, animated: true, completion:nil)
                }
                
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(String(describing: response))")
                
                DispatchQueue.main.async {
                    self.runTakeOrder()
                }
                
            } else {
                
                let jsonResult = try! JSONSerialization.jsonObject(with: data, options: [])
                print("OrderCell: Ответ по запросу к /takeorder = \(jsonResult)")
                let rowData: NSDictionary = jsonResult as! NSDictionary
                let err_id = rowData["err_id"] as! Int
                if err_id == 0 {
                    print("Заказ с ID:\(order_id!) принят")
                    DispatchQueue.main.async {
                        let takenOrder = self.storyboard?.instantiateViewController(withIdentifier: "TakenOrder") as! TakenOrder
                        takenOrder.order_id = "\(order_id)"
                        self.present(takenOrder, animated: false, completion: nil)
                    }
                } else {
                    DispatchQueue.main.async {
                        self.showErrID(err_id: err_id)
                    }
                }
                
            }
        }
        task.resume()
    }
    
}
